# Курс C++

Это основной репозиторий курса. Инструкции по настройке окружения описана в [SETUP.md](docs/SETUP.md). Структура семинарских задач приведена [в тестовой задаче](https://gitlab.com/slon/shad-cpp0/tree/master/multiplication). Задачи типа crashme описаны [здесь](https://gitlab.com/slon/shad-cpp0/blob/master/crash_readme.md).

Перемещаться по задачам и следить за дедлайнами можно тут: https://cpp0.manytask.org/

---
marp: true
---

# Лекция 2. 

Константы
Указатели и ссылки
Передача аргументов в функцию
===

Фёдор Короткий

---

# const
- Ключевое слово (keyword) в С++
- Квалификатор типа (type qualifier)
- Можно дописывать к типам объявляемых переменных
- Компилятор не даст нам поменять значение переменной, объявленной const 


<!--page_number: true-->

---

# Пример

```c++
int x = 0;
x = 1;

const int y = 0;
y = 1;
```

---

# Пример

```c++
int x = 0;
x = 1;

const int y = 0;
y = 1;
```
#

>  error: cannot assign to variable 'y' with **const-qualified type** 'const int'
>
> note: variable 'y' declared const here

---

# Пример

```c++
const double g = 9.8;

...

double E = m*g*h;
```

- Всё глобальные переменные должны быть const-антами

---

# const T vs T const

```c++
int const x = 0;

const int y = 0;
```

---

# Ссылка (reference)
- Способ создать "псевдоним" для переменной
- Для обозначения используется амперсанд (&)

---

# Пример

```c++
int x = 0;
int& y = x;

y = 1;

std::cout << x;
```
#
> 1

---
# Пример

```
std::unordered_map<std::string, int> aVeryLongMap;

std::string aVeryLongKey = "some_string";

int& value = aVeryLongMap[aVeryLongKey];
value = 1;
```

---

# Свойтсва ссылок
- Ссылку необходимо инициализировать при создании
- Ссылка может быть связана с переменной только один раз

---

# Пример

```c++
int x = 0;
int& y = x;

int z = 10;
y = z;
std::cout << x << std::endl;

z = 5;
std::cout << y << std::endl;
```
#
> 10
> 10

---

# Пример

```c++
int x = 0;
const int& ref = x;
ref = 1;

```

> cannot assign to variable 'ref' with const-qualified type 'const int &'

---

# Варианты развития событий

```c++
// копирование
std::string s2 = s1;

// копирования нет, можем менять значение
std::string& s2 = s1;

// копирования нет, можем только читать
const std::string& s2 = s1
```

---

# Какой код быстрее?

```c++
std::vector<std::string> myBooks;

// ... 

// Вариант 1
for (size_t i = 0; i < myBooks.size(); ++i) {
   std::cout << myBooks[i] << std::endl;
}

// Вариант 2
for (std::string book : myBooks) {
   std::cout << book << std::endl;
}

// Вариант 3
for (auto book : myBooks) {
   std::cout << book << std::endl;
}
```

---

# Внутри range-based for

```c++
for (std::string book : myBooks) {
   // код
}

for (size_t i = 0; i < myBooks.size(); ++i) {
   std::string book = myBooks[i];
   // код
}
```

---

# Эффективное итерирование

```c++
for (const auto& element : myVector) {
   // код
}

for (const auto& [key, value] : myMap) {
   // код
}
```

---

# Указатели (pointers)

# ![](scary_pointers.jpg)

---

# Оператор взятия адреса (address-of)

```c++
int x = 42;
std::cout << &x << std::endl;

double pi = 3.1415926;
std::cout << &pi << std::endl;
```
#
> 0x7fff53bd7a98
> 0x7fff53bd7a90
> 
---

# Адрес ссылки

```c++
int x = 0;
std::cout << &x << std::endl;

int& y = x;
std::cout << &y << std::endl;

int z = 1;
std::cout << &z << std::endl;
```
#
> 0x7fff53ecaab8
> 0x7fff53ecaab8
> 0x7fff53ecaab0
---

# Значения амперсанда

```c++
// Побитовое и
auto y = x & 0xDEADBEEF;

// Объявление ссылки
int& y = x;

// Вычисление вдреса
auto addr = &x;
```

---

# Указатели

```c++
int x = 100500;
int* px = &x; 

std::cout << x << std::endl;
std::cout << px << std::endl;
```
#
>100500
>0x7fff50acbab8
---

# Оператор разыменования (dereference)

```c++
int x = 100500;
int* px = &x; 
int y = *px;

std::cout << x << std::endl;
std::cout << y << std::endl;
```
>100500
>100500

---

# Значения звездочки

```c++
// Умножение
auto mul = x * y;

// Объявление указателя
int* px = addr;

// Разыменование указателя
auto y = *px;
```

---

# Как не запутаться

```c++
// Инициализируем указатель адресом переменной
int* addr = &var;

// Инициализируем ссылку переменной по адресу
int& ref = *addr;
```

---

# Где ставить амперсанд и звездочку
#

Вариант L
```c++
int& ref = x;
int* ptr = x;
```
#
Вариант M
```c++
int & ref = x;
int * ptr = x;
```
#
Вариант R
```c++
int &ref = x;
int *ptr = x;
```

--- 

# Изменение значения по указателю

```c++
int x = 5;
int* ptr = &x;

*ptr = 6;

std::cout << x << std::endl;
```
#
> 6

---

# Указатели и ссылки на константы

```c++
const int x = 0; 
int& ref = x;
```
> error: binding value of type 'const int' to reference to type 'int' drops 'const' qualifier
#
```c++
const int x = 0; 
int* ptr = &x;
```
> error: cannot initialize a variable of type 'int *' with an rvalue of type 'const int *'

---

# Указатели и ссылки на константы

```c++
const int x = 0; 
const int& ref = x;
```
#
```c++
const int x = 0; 
const int* ptr = &x;
```

---

# Изменение значения по указателю 

```c++
int x = 0; 
const int* ptr = &x;
x = 1;
```
#
```c++
int x = 0; 
const int* ptr = &x;
*ptr = 1;
```
> error: read-only variable is not assignable

--- 

# Указатель как переменная

```c++
int x = 10;
const int* ptr = &x; 
 
int y = 20;
ptr = &y;
```

---

# Константный указатель

```c++
int x = 10;
int y = 20;
 
int* const ptr = &x; 
ptr = &y;
```
#
> error: cannot assign to variable 'ptr' with const-qualified type 'int *const'

---

# Константный указатель на константу
```c++
int x = 10;

const int* const ptr = &x; 
```
---

# ![](harold.jpg)

---

# Как читать имена типов

```c++
int x; // переменная

const int x; // константа
int const x; // константа
```
#
```c++
int& ref; // обычная ссылка, можно изменять значение 
const int& ref; // константная ссылка, можно только читать
```
#
```c++
int& const ref; // ошибка
```
> 'const' qualifier may not be applied to a reference

---

# Имена типов удобно читать справа налево

```c++
// можно менять значение по адресу
// можно менять указатель
int* ptr = &x;

// нельзя менять значение, можно менять указатель
const int* ptr = &x;
int const* ptr = &x;

// можно менять значение, нельзя менять указатель
int* const ptr = &x;

// нельзя менять значение, нельзя менять указатель
const int* const ptr = &x;
int const* const ptr = &x;
```

---

# Пустой указатель

```c++
double* ptr;

if (condition) {
   ptr = &value;
}

if (ptr) {
   std::cout << *ptr << std::endl;
}
```

---

# nullptr

- Ключевое слово (keyword) для инициализации указателей
- Обозначает, что указатель никуда не указывает
- Нужно использовать вместо NULL
#
```c++
double* ptr = nullptr;
```

---

# Передача аргументов в функцию

```c++
bool IsSorted(std::vector<int> v) {
   // ...
}

// ...

if (IsSorted(v)) {
   std::cout << "It's sorted" << std::endl;
}
```

---

# Что происходит при передаче аргументов?

```c++
int f(std::string s, int n);

// ...

std::string myStr = "abacaba";
int myInt = 10;
std::cout << f(myStr, myInt);

// внутри f:
std::string s = myStr;
int n = myInt;
```

---

# Ссылки и указатели спешат на помощь

```c++
// копируем
int f(std::string s, int n);

// передача по ссылке, не копируем, можем менять
int f(std::string& s, int n);

// передача по константной ссылке
// не копируем, можем только читать
int f(const std::string& s, int n);

// передача по указателю, не копируем, можем менять
int f(std::string* s, int n);

// передача по константному указателю
// не копируем, можем только читать
int f(const std::string* s, int n);
```

---

# Передача аргумента по константной ссылке

С помощью ссылки:
```c++
bool IsSorted(const std::vector<int>& v) {
   // ...
}
```
#
С помощью указателя:
```c++
bool IsSorted(const std::vector<int>* v) {
   // ...
}
```
---

# Что если мы хотим изменить значение?

```c++
void Reverse(std::string s) {
   // ...
}

std:string str = "some string";

Reverse(str);

std::cout << str << std::endl;
```

---

С помощью ссылки:
```c++
void Reverse(std::string& s) {
   // ...
}

Reverse(str);
```
#
C помощью указателя:
```c++
void Reverse(std::string* s) {
   // ...
}

Reverse(&str);
```

---

# Как выглядит вызов?

```c++
Reverse(str) или Reverse(&str);

bool sorted = IsSorted(v);
или
bool sorted = IsSorted(&v);

```

---

# Как выглядит вызов?

```c++
// Передаем указатель - значение переменной
// может изменится
Reverse(&str);

// Передаем по значению или константной ссылке
// значение останется прежним
bool sorted = IsSorted(v);

```

---

#  Типы параметров

- Входные (input)
- Смешанные (in-out)
- Выходные (output)

---

#  Как их передавать?

- Входные
  - Числа - по значению
  - Большие объекты - по константной ссылке
- Смешанные (in-out) - через указатель
- Выходные (output) - через указатель


---

# Зачем нужны выходные параметры?

```c++
bool TryParseInt(const string& s, int* result);

// ...
 
std::string s = "100500";
int result;
if (TryParse(s, &result)) {
   // ...
}
```

---

# C++17 
```c++
std::tuple<bool, int> TryParseInt(const std::string& s);
auto [success, result] = TryParseInt(s);
if (success) {
   // ...
}
```

---

# Слайды*

---

# Жизнь и смерть переменных

```c++
{
   ...
   
   T variable = ... <- начало жизни переменной
   
   ...
   
   <- конец жизни переменной
}
```

---

# Жизнь и смерть переменных

```c++
void f(const std::vector<int>& v) {	
   int sum2 = 0; 			// 1
					// 2
   for (const auto& x : v) {		// 3
      int x2 = x*x; 			// 4
      sum2 += x2;			// 5
   }					// 6
   					// 7
   std::cout << sum2 << std::endl; 	// 8
}
```

--- 

# ![](shotgun.jpg)

---

# Испорченный указатель (dangling pointer)

```c++
int* ptr = nullptr;

{
   int x = 100500;
   std::cout << x << std::endl;
   ptr = &x;
}

std::cout << *ptr << std::endl;
```

---
```c++
int* ptr = nullptr;

{
   int x = 100500;
   std::cout << x << std::endl;
   ptr = &x;
}

std::cout << *ptr << std::endl;
```
#
> 100500
> 100500 (скорее всего)
#
> AddressSanitizer: stack-use-after-scope on address ...

---

# Испорченная ссылка (dangling reference)

```c++
std::vector<int> v = {1, 2, 3, 4, 5};

int& ref = v[2];
ref = 10;

for (const auto& x : v) {
   std::cout << x << " ";
}
std::cout << std::endl;

v.clear();

std::cout << ref << std::endl;
```

---

```c++
std::vector<int> v = {1, 2, 3, 4, 5};

int& ref = v[2];
ref = 10;

for (const auto& x : v) {
   std::cout << x << " ";
}
std::cout << std::endl;

v.clear();

std::cout << ref << std::endl;
```
#
> 1 2 10 4 5 
> 10 (скорее всего)
#
> AddressSanitizer: container-overflow on address ...

---

```c++
int& GetX() {
   int x = 10;
   return x;
}

// ...

std::cout << GetX()<< std::endl;
```
#
> AddressSanitizer: stack-use-after-return on address
#
ASAN_OPTIONS=detect_stack_use_after_return=1

---

# NB: нельзя возвращать из функций ссылки и указатели на локальные переменные

---

# Слайды**

--- 

```c++
void f(int* ptr) {
   std::cout << *ptr << std::endl;

   int t = 20;
   ptr = &t;

   std::cout << *ptr << std::endl;
}
```
#
```c++
int x = 10;
int* ptr = &x;
f(ptr);

std::cout << *ptr << std::endl;
```

---

> 10
> 20
> 10

#
#

```c++
int* ptr_1 = &x; // до вызова функции
int* ptr_2 = ptr_1; // внутри функции
...
ptr_2 = &t;
```

---

# Ссылка на указатель?

--- 

# Полезные ссылки (просто ссылки)

https://habr.com/post/414443/

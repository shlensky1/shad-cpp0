# Взаимодействие с C

Короткий Фёдор

---

# Что такое С?

 - Появился в 1972, 45 лет назад.
 - Самый переносимый язык. Библиотеки - нет.
 - Системные требования: процессор, память.
 - Важнейшая часть UNIX-like операционных систем.
 - Python, Ruby, Perl - написаны на C.
 - opengl
 - mysql, postgresql

---

# Зачем вам знать C?

 - Взаимодействие с OS
 - Сложные форматы данных: png, jpeg, xml
 - Архиваторы: gzip, lzma
 - Python C API
 - Numerical Libraries

---

# Знаю С++ != Знаю С

 - Язык С++ проектировался _синтаксически_ совместимым с языком C.
 - Идиоматичный код на C кардинально отличается от кода C++

---

# `C++--`

 - Нет деструкторов `~Object();`
 - Нет конструкторов, методов, классов
 - Нет исключений `throw;`
 - Нет ссылок `int&`
 - Нет шаблонов, нет `std::vector`
 - Нет перегрузки
 - Нет `new` / `delete`

---

# Нет классов

 - Язык поддерживает структуры
   ```c
   struct File {
       int fd;
       char* buf;
   };
   ```
 - Все поля публичные
 - `a = b;` - **побитовая** копия, нет пользовательского копирования
 - Нет классов _на уровне языка_, ООП идеи используются в коде

---

# C классы

```c
FILE* fopen(const char *pathname, const char *mode);
size_t fread(void *ptr, size_t size,
             size_t nmemb, FILE *stream);
int fclose(FILE* stream);
```

 - Конструктор и деструктор - отдельные функции
 - Имена функция начинаются с общего префикса

```c++
FILE* f = fopen("text.txt", "r");
free(f); // WRONG!
````

 - Нужно использовать `std::shared_ptr` c пользовательским `Deleter`-ом.

```c++
std::shared_ptr<FILE> f(fopen("test.txt", "r"), fclose);
```

---

# Spot the `class`

```c
int sqlite3_open(
  const char *filename,
  sqlite3 **ppDb
);
int sqlite3_exec(
  sqlite3*,
  const char *sql,
  int (*callback)(void*,int,char**,char**),
  void *,
  char **errmsg
);
int sqlite3_close(sqlite3*);
```

---

# Нет исключений

 - Обработка ошибок через error-коды
 - Нужно проверять возвращаемое значение **каждой** функции
 - Сообщение об ошибке получается отдельной функцией
 - Вместо автоматической раскрутки стека используется паттерн `goto err`

---

# C-строки

 - `char*` - `'\0' terminated` последовательность байт
 - Нет семантики владения:
   - В разных функцих может быть разной
   - Обычно владение не передаётся
 - `std::string` - совместима. `.c_str()`
 - `char*` - **не значит**, что строка будет `zero-terminated`.
 - `size_t read(char* buf, size_t size);`

---

# C-строки - частые ошибки

```c++
char* get_buf(FOO* );
size_t get_size(FOO* );

std::string buf = get_buf(foo); // WRONG
```

---

# C-строки - частые ошибки

```c++
char* get_buf(FOO* );
size_t get_size(FOO* );

std::string buf(get_buf(foo), get_size(foo));
```

---

# C-строки - частые ошибки

```c
char buf[128];
read(fd, buf, 128);
```

---

# C-строки - частые ошибки

```c
char buf[128];
read(fd, buf, 127);
buf[127] = 0;
printf(buf); // wrong
```

---

# C-строки - частые ошибки

```c
char buf[128];
read(fd, buf, 127);
buf[127] = 0;
printf("%s", buf);
```

---

# `char*` - различные смыслы

```
size_t strlen(char* buf);
size_t strlen(const std::string& str);
```

```
size_t read(char* buf, size_t size);
size_t read(std::string_view buf);
```

```
char* strdup(const char* s);
std::string strdup(const std::string& s);
```

```
char* PyArray_DATA(PyArrayObject *arr);
int* std::vector<int>::data();
```

```
char* getenv(char* name);
const std::string& getenv(const std::string& name);
```

---

# Перегрузка и линковка

```
// f.c
int f() {
    return 42;
}

// f.h
int f();

// main.cpp
#include "f.h"

int main() {
    std::cout << f() << std::endl;
    return 0;
}

// main.cpp:(.text+0x5): undefined reference to `f()'
```

---

# Перегрузка и линковка

 - C++ calling convention совместимо с С
 - Есть несовместимость на уровне линкера из-за перегрузок
 - `void f(int); void f(double)` - невозможно в C
 - Решение `extern "C" void f(int);`

```
#ifdef __cplusplus
extern "C" {
#endif

void f(int);

#ifdef __cplusplus
}
#endif
```

---

# C-callback

```
int pthread_create(pthread_t *thread,
                   const pthread_attr_t *attr,
                   void *(*start_routine) (void *),
                   void *arg);

struct MyThread {
    static void Start(void* arg) {
        auto this = (MyThread*)arg;
        arg->Run();
    }
    
    void Run();
};

pthread_t id;
MyThread thread;
pthread_create(&id, nullptr, &MyThread::Start, &thread);
```

---

# Правильное взаимодействие с C

 * Обернуть С объекты в умный указатели / владеющие классы
 * Перевести ошибки в исключения
 * Добавить type-safe хелперы для работы с `void*`

---

# Python C API

 - Можно писать модули для python на C++
 - Будем рассказывать завтра на лекции по питону
